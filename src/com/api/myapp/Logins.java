package com.api.myapp;

import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONStringer;

import com.api.cfg.dbtoquery;
import com.api.libs.ToJSONObject;


/**
 * Servlet implementation class Logins
 */
//@WebServlet(asyncSupported = true, urlPatterns = { "/logins" })
public class Logins {
	private final long serialVersionUID = 1L;
	private BufferedReader reader;
	private ToJSONObject toObjectJson;
	private JSONObject jsonObject;
	private Object resAkhir;
	private dbtoquery dbtoquerys;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Logins() {
        super();
        // TODO Auto-generated constructor stub
        reader = null;
        toObjectJson = null;
        jsonObject = null;
        resAkhir = null;
        dbtoquerys = null;
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(Object result, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append(result.toString());
		response.reset();
		response.getWriter().flush();
		response.getWriter().close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public Object doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		reader = request.getReader();
		toObjectJson = new ToJSONObject();
		jsonObject = toObjectJson.toJSONObject(reader);
		try {
			dbtoquerys = new dbtoquery();
			resAkhir = dbtoquerys.getLogin(jsonObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		reader.reset();
		reader.close();
		request.getReader().close();
		request.logout();
		return resAkhir;
	}

}
