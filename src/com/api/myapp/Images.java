/**
 * 
 */
package com.api.myapp;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.api.cfg.dbtoquery;
import com.api.libs.ToJSONObject;

/**
 * @author Bagus Anggoro
 *
 */
public class Images {
	
	private BufferedImage originalImage, bi,  ret, subimage, resizeImage, croppedImage, subimagex;
	private  Image bis, scaledImage;
	private  Graphics2D g;
	private  int width, height, left, top, widthmax, heightmax, leftmax, topmax;
	private  int types;
	private  double d;
	private  final String DENSITY_UNITS_NO_UNITS = "00";
	private  final String DENSITY_UNITS_PIXELS_PER_INCH = "01";
	private  final String DENSITY_UNITS_PIXELS_PER_CM = "02";
	private BufferedReader reader;
	private ToJSONObject toObjectJson;
	private JSONObject jsonObject;
	private Object resAkhir;   
	private dbtoquery dbtoquerys;
	private IIOImage outputImage;
	
	public Images() {
        super();
        // TODO Auto-generated constructor stub
        reader = null;
        toObjectJson = null;
        jsonObject = null;
        resAkhir = null;
        dbtoquerys = null;
    }
    
	/**
	 * @return 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public Object doPost(HttpServletRequest request1, HttpServletResponse response1) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		reader = request1.getReader();
		toObjectJson = new ToJSONObject();
		jsonObject = toObjectJson.toJSONObject(reader);
//		System.out.println(jsonObject.toString());
//		System.out.println(jsonObject.get("types").toString());
//		System.out.println(jsonObject.get("filesnya").toString());
//		System.exit(0);
		types = Integer.parseInt(jsonObject.get("types").toString());
		
		if(types == 1 ){
//			saveSetDPI(new File(args[1]));
			bi = cropBody(jsonObject.get("filesnya").toString()); // method untuk crop body dengan kordinat
		}else if(types != 1 ){
//			saveSetDPI(new File(args[1]));
			bi = cropHeader(jsonObject.get("filesnya").toString()); // method untuk crop header dengan kordinat
		}
		
//		System.out.println(args[2]);
//		System.out.println(args[3]);
        width = Math.max(0, bi.getWidth());
        height = Math.max(0, bi.getHeight());
        
        widthmax = Integer.parseInt(jsonObject.get("width").toString()); //2
        heightmax = Integer.parseInt(jsonObject.get("height").toString()); //3
        
        if (width <= 0) {
            width = bi.getWidth();
        }
        
        if (height <= 0) {
            height = bi.getHeight();
        }

        left = Math.min(Math.min(0, bi.getWidth()), bi.getWidth() - width);
        top = Math.min(Math.min(0, bi.getHeight()), bi.getHeight() - height);
        
        toGrayScale(); // methd agar berwarna tampak lebih gelap
        subimage = bi.getSubimage(left, top, width, height);
        resizeImage = resizeFile(subimage, widthmax, heightmax);
        System.out.println(jsonObject.get("save").toString());
        save(resizeImage, jsonObject.get("save").toString()); // method untuk save file image.. // 4
        
        return "Data Berhasil !!!";
		
	}
	
	private BufferedImage resizeFile(BufferedImage subimage2, int reqWidth, int reqHeight){
		
		scaledImage = bi.getScaledInstance(Math.max(0, reqWidth), Math.max(0, reqHeight), Image.SCALE_SMOOTH);
        ret = new BufferedImage(Math.max(0, reqWidth), Math.max(0, reqHeight), BufferedImage.TYPE_INT_RGB);
        g = ret.createGraphics();
        float dash1[] = {10.0f};
        BasicStroke dashed =
                new BasicStroke(1.0f,
                                BasicStroke.CAP_BUTT,
                                BasicStroke.JOIN_MITER,
                                10.0f, dash1, 0.0f);
        g.setStroke(dashed);
        g.drawImage(scaledImage, 0, 0, null);
        return ret;
    }
	
	private void save(BufferedImage bi, String outputImageFile) {
        try {
        	System.out.println(outputImageFile);
        	saveSetDPI(new File(outputImageFile));
        	ImageWriter jpgWriter = ImageIO.getImageWritersByFormatName("jpg").next();
            ImageWriteParam jpgWriteParam = jpgWriter.getDefaultWriteParam();
            jpgWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            jpgWriteParam.setCompressionQuality(1f);
            
            jpgWriter.setOutput(ImageIO.write(bi, getExtName(outputImageFile), new File(outputImageFile)));
            outputImage = new IIOImage(bi, null, null);
            jpgWriter.write(null, outputImage, jpgWriteParam);
            jpgWriter.dispose();
            
            
//            ImageIO.write(bi, getExtName(outputImageFile), new File(outputImageFile));
        } catch (Exception e) {
            e.getMessage();
        }
    }
	
	private String getExtName(String fileName) {
        int index = fileName.lastIndexOf('.');
        if (index != -1 && (index + 1) < fileName.length()) {
            return fileName.substring(index + 1);
        } else {
            return null;
        }
    }
	
	private void toGrayScale() {

		//convert to grayscale
	    for(int y = 0; y < height; y++){
	      for(int x = 0; x < width; x++){
	        int p = bi.getRGB(x,y);

	        int a = (p>>24)&0xff;
	        int r = (p>>16)&0xff;
	        int g = (p>>8)&0xff;
	        int b = p&0xff;

	        //calculate average
	        int avg = (r+g+b)/3;

	        //replace RGB value with avg
	        p = (a<<24) | (avg<<16) | (avg<<8) | avg;

	        bi.setRGB(x, y, p);
	      }
	    }

    }
	
	private void processImg(BufferedImage ipimage, 
            float scaleFactor, 
            float offset){
		RescaleOp rescale = new RescaleOp(scaleFactor, offset, null); 
		rescale.filter(ipimage, null); 
	}
	
	private BufferedImage cropBody(String file) throws IOException {
        originalImage = ImageIO.read(new File(file));
//        d = originalImage 
//              .getRGB(originalImage.getWidth() / 2, 
//            		  originalImage.getHeight() / 2);
//        if (d >= -1.4211511E7 && d < -7254228) { 
//            processImg(originalImage, 3f, -10f); 
//        } 
//        else if (d >= -7254228 && d < -2171170) { 
//            processImg(originalImage, 1.455f, -47f); 
//        } 
//        else if (d >= -2171170 && d < -1907998) { 
//            processImg(originalImage, 1.35f, -10f); 
//        } 
//        else if (d >= -1907998 && d < -257) { 
//            processImg(originalImage, 1.19f, 0.5f); 
//        } 
//        else if (d >= -257 && d < -1) { 
//            processImg(originalImage, 1f, 0.5f); 
//        } 
//        else if (d >= -1 && d < 2) { 
//            processImg(originalImage, 1f, 0.35f); 
//        } 
        
//        RescaleOp rescaleOp = new RescaleOp(1.2f, 15, null);
//		rescaleOp.filter(originalImage, null);
        
        processImg(originalImage, 1.2f, 15);
        
        
        
        int height = originalImage.getHeight();
        int width = originalImage.getWidth();

        int targetWidth = (int) width;
        int targetHeight = (int)(( height / 4) + 33);
        
        // Coordinates of the image's middle
        int xc = 0;
        int yc = 253;

        // Crop
        croppedImage = originalImage.getSubimage(
                        xc, 
                        yc,
                        targetWidth, // widht
                        targetHeight // height
        );
        return croppedImage;
    }
	
	private BufferedImage cropHeader(String file) throws IOException {
        originalImage = ImageIO.read(new File(file));
//        d = originalImage 
//                .getRGB(originalImage.getTileWidth() / 2, 
//              		  originalImage.getTileHeight() / 2);
//      if (d >= -1.4211511E7 && d < -7254228) { 
//      processImg(originalImage, 3f, -10f); 
//  } 
//  else if (d >= -7254228 && d < -2171170) { 
//      processImg(originalImage, 1.455f, -47f); 
//  } 
//  else if (d >= -2171170 && d < -1907998) { 
//      processImg(originalImage, 1.35f, -10f); 
//  } 
//  else if (d >= -1907998 && d < -257) { 
//      processImg(originalImage, 1.19f, 0.5f); 
//  } 
//  else if (d >= -257 && d < -1) { 
//      processImg(originalImage, 1f, 0.5f); 
//  } 
//  else if (d >= -1 && d < 2) { 
//      processImg(originalImage, 1f, 0.35f); 
//  } 
  
//  RescaleOp rescaleOp = new RescaleOp(1.2f, 15, null);
//	rescaleOp.filter(originalImage, null);
  
        processImg(originalImage, 1.2f, 15);
        
        int height = originalImage.getHeight();
        int width = originalImage.getWidth();

        int targetWidth = (int) width;
        int targetHeight = (int)((height / 4) + 40);
        // Coordinates of the image's middle
        int xc = 0;
        int yc = 0;

        // Crop
        croppedImage = originalImage.getSubimage(
                        xc, 
                        yc,
                        targetWidth, // widht
                        targetHeight // height
        );
        return croppedImage;
    }
	
	private void saveSetDPI(File output) throws IOException {
	    output.delete();

	    final String formatName = "jpeg";

	    for (Iterator<ImageWriter> iw = ImageIO.getImageWritersByFormatName(formatName); iw.hasNext();) {
	        ImageWriter writer = iw.next();
	        ImageWriteParam writeParam = writer.getDefaultWriteParam();
	        ImageTypeSpecifier typeSpecifier = ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_RGB);
	        IIOMetadata metadata = writer.getDefaultImageMetadata(typeSpecifier, writeParam);
	        if (metadata.isReadOnly() || !metadata.isStandardMetadataFormatSupported()) {
	            continue;
	        }

	        setDPI(metadata);

	        final ImageOutputStream stream = ImageIO.createImageOutputStream(output);
	        try {
	            writer.setOutput(stream);
	            writer.write(metadata, new IIOImage(bi, null, metadata), writeParam);
	        } finally {
	            stream.close();
	        }
	        break;
	    }
	}

	private void setDPI(IIOMetadata metadata) throws IIOInvalidTreeException {
	    String metadataFormat = "javax_imageio_jpeg_image_1.0";
	    IIOMetadataNode root = new IIOMetadataNode(metadataFormat);
	    IIOMetadataNode jpegVariety = new IIOMetadataNode("JPEGvariety");
	    IIOMetadataNode markerSequence = new IIOMetadataNode("markerSequence");
	    
	    final int metersToInches = 40;//39.3701;
        int dotsPerMeter = (int) 300 * metersToInches; //Math.round(300 * metersToInches);
	    
	    IIOMetadataNode app0JFIF = new IIOMetadataNode("app0JFIF");
	    app0JFIF.setAttribute("majorVersion", "1");
	    app0JFIF.setAttribute("minorVersion", "2");
	    app0JFIF.setAttribute("thumbWidth", "0");
	    app0JFIF.setAttribute("thumbHeight", "0");
	    app0JFIF.setAttribute("resUnits", DENSITY_UNITS_PIXELS_PER_INCH);
	    app0JFIF.setAttribute("Xdensity", String.valueOf(dotsPerMeter));
	    app0JFIF.setAttribute("Ydensity", String.valueOf(dotsPerMeter));

	    root.appendChild(jpegVariety);
	    root.appendChild(markerSequence);
	    jpegVariety.appendChild(app0JFIF);

	    metadata.mergeTree(metadataFormat, root);
	}
}
