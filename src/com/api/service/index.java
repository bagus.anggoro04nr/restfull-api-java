package com.api.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.api.myapp.Absen;
import com.api.myapp.Barang;
import com.api.myapp.Info;
import com.api.myapp.Item;
import com.api.myapp.Logins;
import com.api.myapp.Menus;
import com.api.myapp.Modals;
import com.api.myapp.Reports;
import com.api.myapp.SendDataImages;
import com.api.myapp.ShowTable;
import com.api.myapp.UpdateData;
import com.api.myapp.Users;

/**
 * Servlet implementation class index
 */
@WebServlet(asyncSupported = true, urlPatterns = { "/" })
public class index extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logins login;
	private Menus menu;
	private ShowTable st;
	private Reports rpt;
	private Modals mods;
	private UpdateData upd;
	private SendDataImages sdi;
	private Item itm;
	private Users usr;
	private Info info;
	private Absen abs;
	private Barang brg;
	
	private Object getData;
	private String path;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(Object request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		  
		// Set to expire far in the past.
		response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
	
		// Set standard HTTP/1.1 no-cache headers.
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
	
		// Set IE extended HTTP/1.1 no-cache headers (use addHeader).
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
	
		// Set standard HTTP/1.0 no-cache header.
		response.setHeader("Pragma", "no-cache");
		  
		response.getWriter().append(request.toString());
		response.getWriter().flush();
		response.getWriter().close();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}
	
	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
	    path = req.getServletPath();
	    switch (path) {
	      case "/logins":
	    	  login = new Logins();
	    	  getData = login.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/menu":
	    	  menu = new Menus();
	    	  getData = menu.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/showtables":
	    	  st = new ShowTable();
	    	  getData = st.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/reports":
	    	  rpt = new Reports();
	    	  getData = rpt.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/modals":
	    	  mods = new Modals();
	    	  getData = mods.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/updates":
	    	  upd = new UpdateData();
	    	  getData = upd.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/senddataimages":
	    	  sdi = new SendDataImages();
	    	  getData = sdi.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/item":
	    	  itm = new Item();
	    	  getData = itm.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/user":
	    	  usr = new Users();
	    	  getData = usr.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/absen":
	    	  abs = new Absen();
	    	  getData = abs.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/info":
	    	  info = new Info();
	    	  getData = info.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      case "/barang":
	    	  brg = new Barang();
	    	  getData = brg.doPost(req, resp);
	    	  doGet(getData, resp);
	        break;
	      default:
	    	  Object datadef = req.getContextPath().toString();
	    	  doGet(datadef, resp);
	    }

	    // do something else
	  }

}
