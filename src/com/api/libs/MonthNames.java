/**
 * 
 */
package com.api.libs;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bagus Anggoro
 *
 */
public class MonthNames {
	private String[] months;
	private String[] shortMonths;
	private List<Map<String, Object>> monthnya;
	private Map<String, Object> row;
	private String[] shortMonth;
	
	public MonthNames() {
		super();
		months = new DateFormatSymbols().getMonths();
		shortMonths = new DateFormatSymbols().getShortMonths();
		monthnya = new ArrayList<Map<String, Object>>();
		row = new HashMap<String, Object>();
	}
	
	public List<Map<String, Object>> getMontNames() {
		

		for (int i = 0; i < months.length; i++) {
			
			System.out.println(months[i].toString());
			row.put(String.valueOf(i), months[i].toString());
	       
	    }
		monthnya.add(row);
		return monthnya;
	}
	
	public String[] getShortMontNames() {
		
		for (int i = 0; i < shortMonths.length; i++) {
			shortMonth[i] = shortMonths[i];
	        
	    }
		
		return shortMonth;
	}
	
	public List<Map<String, Object>> setGetMontNames(String bulan) {
		System.out.println(bulan);
		
		if(setgetdate.getMonthPrinted().toString().contains("fullMonth")) {
			for (int i = 0; i < months.length; i++) {
				
				
				row.put(String.valueOf(i), months[i].toString());
				if(months[i].toString().matches(bulan)) break;
		       
		    }
		}else row.put(String.valueOf(0), bulan);
		
		monthnya.add(row);
		return monthnya;
	}

}
