/**
 * 
 */
package com.api.documents;

import java.awt.HeadlessException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.json.JSONException;

import com.api.libs.pathfile;
import com.api.libs.servers;
import com.api.libs.setgetdate;
import com.api.libs.typesnya;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @author ramad
 *
 */
public class Create_Documents {

	private String pathPDF="", pathXLS="", kodePO="", pathFile="", pathFile1= "", items="", hargas1="", TotalHarga2="", quantity1="", TotalHarga="";
	private Document document;
	private String namaFiles="", items1="", hargas="", quantity="", pathAll="";
	private PdfWriter writer;
	private PdfPTable table, table2, table3;
	private Font boldFont, StandardFont, TotalFont;
	private int nourut, numbers1, noNya, noNya1, noNyas12, poKode, poKode1, poKode2;
	private int TotalQty, poKode12, hargaTotal, TotalQty1, TotalHarga1, noNyas121;
	private int ttlQty, ttlHarga, numbers, ttlQty1, ttlHarga1, noNya2, noNyas, noNyas1, noNyas3;
	private Workbook workbook;
	private Sheet sheet;
	private Row rowBarang, row, rows, rowBarangs, rowBarangs1, rowBarangs2;
	private Cell cell, cell1, cellttl1, cellttl, cellttl2;
	private CellStyle styles1, style, style1;
	private String formula, formula2, formula1;
	private int nilas, nilas1;
	
	public Create_Documents() {
		super();
		nilas = 0;
		nilas1 = 0;
		ttlQty=0;
		ttlHarga=0;
		numbers=0;
		ttlQty1=0;
		ttlHarga1=0;
		noNya2=0;
		noNyas=0;
		noNyas1=0; 
		noNyas3=0;
		TotalQty=0;
		poKode12=0;
		hargaTotal=0;
		TotalQty1=0;
		TotalHarga1=0;
		noNyas121=0;
		nourut=0;
		numbers1=2;
		noNya=0;
		noNya1=0;
		noNyas12=0;
		poKode=0;
		poKode1=0;
		poKode2=0;
	}
	
	private  String namaFile(String types){
		
		if(setgetdate.getDateFrom().matches(".*\\d+.*") & setgetdate.getDateTo().matches(".*\\d+.*")){
			namaFiles = "Reports_" + types + "_" + setgetdate.getDateFrom() + "_" + setgetdate.getDateTo();
		}
		
		return namaFiles;
		 
	}
	
	public  Object createToPDF(List<Map<String, Object>> result) throws DocumentException, JSONException, SQLException, HeadlessException, UnsupportedFlavorException, IOException {
		pathPDF = "";
		document = new Document();
		pathFile = pathfile.getPathFile() + namaFile(typesnya.getType().toUpperCase()) + ".pdf".trim();
		pathPDF = servers.getHostname().toString() + pathfile.getPathFile1() + namaFile(typesnya.getType().toUpperCase()) + ".pdf".trim();
//		pathPDF = pathPDF.replace("/../", "");
		System.out.println(pathFile);
		System.out.println(pathPDF);
		writer = PdfWriter.getInstance(document, new FileOutputStream(pathFile));
		
//		writer.setEncryption(USER_PASSWORD.getBytes(),
//                OWNER_PASSWORD.getBytes(), PdfWriter.ALLOW_PRINTING,
//                PdfWriter.ENCRYPTION_AES_128);
		
	    document.open();
	    document.add(new Paragraph("Reports Print Date : " + setgetdate.giveDateNowReport()));
	    document.add(new Paragraph(""));
	    document.add(new Paragraph("Reports " + typesnya.getType().toUpperCase()));
	    
	    //Set attributes here
	    document.addAuthor("iseng - iseng");
	    document.addCreationDate();
	    document.addCreator("main aja");
	    document.addTitle("Reports");
	    document.addSubject("Report" + typesnya.getType().toUpperCase());
        
        //Set Column widths
	   
        float[] columnWidths = {1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f};
        float[] totalcolumnWidths = {1f, 1f, 1f, 1f};
        
        boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
        StandardFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
        TotalFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLUE );
        
        table = new PdfPTable(columnWidths); // 6 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(5f); //Space before table
        table.setSpacingAfter(5f); //Space after table
        table.setWidths(columnWidths);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setPaddingLeft(5);
        table.getDefaultCell().setBorderColor(BaseColor.BLACK);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.getDefaultCell().setBorder(1);
	    table.addCell(new Phrase("NO", boldFont ));
	    table.addCell(new Phrase("Number PO", boldFont ));
	    table.addCell(new Phrase("Customer", boldFont ));
	    table.addCell(new Phrase("Alamat Customer", boldFont ));
	    table.addCell(new Phrase("Nama Barang", boldFont ));
	    table.addCell(new Phrase("Quantity", boldFont ));
	    table.addCell(new Phrase("Harga", boldFont ));
	    table.addCell(new Phrase("Harga Total", boldFont ));
	    
	    document.add(table);
	    
	    if(!result.isEmpty()){
	    
	        table2 = new PdfPTable(columnWidths); // 6 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        table2.setSpacingBefore(5f); //Space before table
	        table2.setSpacingAfter(5f); //Space after table
	        table2.setWidths(columnWidths);
	        table2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
	        table2.getDefaultCell().setPaddingLeft(5);
	        table2.getDefaultCell().setBorderColor(BaseColor.BLACK);
	        table2.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
	        table2.getDefaultCell().setBorder(1);
	        
	        for (int i = 0; i < result.size(); i++) {
	        	
	        	String kode_po = result.get(i).get("kode_po").toString();
	        	items1="";
	        	hargas1="";
	        	quantity1="";
	        	TotalQty1=0;
	        	TotalHarga1=0;
	        	
	        	TotalHarga2 = "";
	        	if(!kodePO.matches(kode_po)){
	        		nourut++;
	            	String customer = result.get(i).get("customer").toString();
	            	String alamat_cust = result.get(i).get("alamat_cust").toString();
	            	table2.addCell(new Phrase(String.valueOf(nourut), StandardFont));
	             	table2.addCell(new Phrase(kode_po, StandardFont));
	             	table2.addCell(new Phrase(customer, StandardFont));
	             	table2.addCell(new Phrase(alamat_cust, StandardFont));
	             	 for (int a = 0; a < result.size(); a++) {
	             		String kode_ponya = result.get(a).get("kode_po").toString();
	             		
	             		if(kode_po.matches(kode_ponya)){
	             			items= "";
	             			hargas="";
	             			quantity="";
	             			TotalHarga="";
	             			quantity = "@ " + result.get(a).get("jmlh_barang").toString() + " \n\r";
	             			items = "@ " + result.get(a).get("nama_barang").toString() + " \n\r";
	    	                hargas = "@ " + result.get(a).get("harga_beli").toString() + " \n\r";
	    	                items1 = items1.concat(items);
	    	              	hargas1 = hargas1.concat(hargas);
	    	              	quantity1 = quantity1.concat(quantity);
	    	              	TotalQty1 += Integer.valueOf(result.get(a).get("jmlh_barang").toString());
	    	              	TotalHarga1 += Integer.valueOf(result.get(a).get("jmlh_barang").toString()) * Integer.valueOf(result.get(a).get("harga_beli").toString()) / 1.000;
	    	              	TotalQty += Integer.valueOf(result.get(a).get("jmlh_barang").toString());
	    	              	TotalHarga = "@ " + Integer.valueOf(result.get(a).get("jmlh_barang").toString()) * Integer.valueOf(result.get(a).get("harga_beli").toString()) + " \n\r";
	    	              	TotalHarga2 = TotalHarga2.concat(TotalHarga);
	    	              	hargaTotal += Integer.valueOf(result.get(a).get("jmlh_barang").toString()) * Integer.valueOf(result.get(a).get("harga_beli").toString());
	    	              	
	             		}
	             	 }
	             	
	               	
	             	table2.addCell(new Phrase(items1, StandardFont));
	             	table2.addCell(new Phrase(quantity1, StandardFont));
	               	table2.addCell(new Phrase(hargas1, StandardFont));
	               	table2.addCell(new Phrase(String.valueOf(TotalHarga2), StandardFont));
	               	
	               	PdfPCell cell1 = new PdfPCell(new Phrase(String.valueOf(TotalQty1), TotalFont));
	                cell1.setBorderColor(BaseColor.BLACK);
	                cell1.setPaddingLeft(10);
	                cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
	                cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	                cell1.setBackgroundColor(BaseColor.PINK);
	                cell1.setBorder(0);
	         
	                PdfPCell cell2 = new PdfPCell(new Phrase(String.valueOf(TotalHarga1), TotalFont ));
	                cell2.setBorderColor(BaseColor.BLACK);
	                cell2.setPaddingLeft(5);
	                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
	                cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
	                cell2.setBackgroundColor(BaseColor.PINK);
	                cell2.setBorder(0);
	                
	                PdfPCell cell3 = new PdfPCell(new Phrase(""));
	                cell3.setBackgroundColor(BaseColor.PINK);
	                cell3.setBorder(0);
	                
	                PdfPCell cell4 = new PdfPCell(new Phrase(""));
	                cell4.setBackgroundColor(BaseColor.PINK);
	                cell4.setBorder(0);
	                
	                PdfPCell cell5 = new PdfPCell(new Phrase(""));
	                cell5.setBackgroundColor(BaseColor.PINK);
	                cell5.setBorder(0);
	                
	                PdfPCell cell6 = new PdfPCell(new Phrase(""));
	                cell6.setBackgroundColor(BaseColor.PINK);
	                cell6.setBorder(0);
	                
	                PdfPCell cell7 = new PdfPCell(new Phrase("Total Qty", boldFont));
	                cell7.setBorderColor(BaseColor.BLACK);
	                cell7.setPaddingLeft(5);
	                cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
	                cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
	                cell7.setBackgroundColor(BaseColor.PINK);
	                cell7.setBorder(0);
	                
	                PdfPCell cell8 = new PdfPCell(new Phrase("Total Harga", boldFont ));
	                cell8.setBorderColor(BaseColor.BLACK);
	                cell8.setPaddingLeft(5);
	                cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
	                cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);
	                cell8.setBackgroundColor(BaseColor.PINK);
	                cell8.setBorder(0);
	               	
	               	table2.addCell(cell3);
	            	table2.addCell(cell4);
	            	table2.addCell(cell7);
	            	table2.addCell(cell1);
	            	table2.addCell(cell8);
	            	table2.addCell(cell2);
	            	table2.addCell(cell5);
	               	table2.addCell(cell6);
	        	}
	        	
	        	kodePO = kode_po;
	        }
	        	
	        document.add(table2);
	        
	        table3 = new PdfPTable(totalcolumnWidths); // 4 columns.
	        table3.setWidthPercentage(100); //Width 100%
	        table3.setSpacingBefore(5f); //Space before table
	        table3.setSpacingAfter(5f); //Space after table
	        table3.setWidths(totalcolumnWidths);
	        table3.getDefaultCell().setBackgroundColor(BaseColor.YELLOW);
	        table3.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
	        table3.getDefaultCell().setPaddingLeft(5);
	        table3.getDefaultCell().setBorderColor(BaseColor.BLACK);
	        table3.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
	        table3.getDefaultCell().setBorder(1);
	        table3.addCell(new Phrase("Total Qty", boldFont ));
	        table3.addCell(new Phrase(String.valueOf(TotalQty), TotalFont));
	        table3.addCell(new Phrase("Total Harga", boldFont ));
	        table3.addCell(new Phrase(String.valueOf(hargaTotal), TotalFont));
	        
		    document.add(table3);
	    
	    }
        
        document.close();
        writer.close();
        
        return pathPDF;
	}
	
	public  Object createToXLS(List<Map<String, Object>> result2) throws DocumentException, JSONException, SQLException, HeadlessException, UnsupportedFlavorException, IOException {
		pathXLS = "";
		pathFile1 = pathfile.getPathFile() + namaFile(typesnya.getType().toUpperCase()) + ".xls".trim();
		pathXLS = servers.getHostname().toString() + pathfile.getPathFile1() + namaFile(typesnya.getType().toUpperCase()) + ".xls".trim();
//		pathXLS = pathXLS.replace("/../", "");
		
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(namaFile(typesnya.getType().toUpperCase()));
		createHeaderRow(sheet);

	    for (int c = 0; c < result2.size(); c++) {
	    	String kode_po = result2.get(c).get("kode_po").toString();
	    	
	    	
			if(!kodePO.matches(kode_po)){
				numbers++;
				
				style = workbook.createCellStyle();
	            /* First, let us draw a thick border so that the color is visible */            
	    	    style.setBorderLeft(BorderStyle.THICK);             
	    	    style.setBorderRight(BorderStyle.THICK);            
	    	    style.setBorderTop(BorderStyle.THICK);              
	    	    style.setBorderBottom(BorderStyle.THICK);
	    	    style.setAlignment(HorizontalAlignment.CENTER);
	    	    style.setVerticalAlignment(VerticalAlignment.CENTER);
	            /* We will use IndexedColors to specify colors to the border */
	            /* bottom border color */
	    	    style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
	            /* Top border color */
	    	    style.setTopBorderColor(IndexedColors.BLACK.getIndex());
	            /* Left border color */
	    	    style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
	            /* Right border color */
	    	    style.setRightBorderColor(IndexedColors.BLACK.getIndex());
	    	    
	    	    
	    		style1 = workbook.createCellStyle();
	    		
	    		org.apache.poi.ss.usermodel.Font font1 = workbook.createFont();
	            font1.setColor(HSSFColor.HSSFColorPredefined.PINK.getIndex());
	            style1.setFont(font1); 
	            
				style1.setBorderLeft(BorderStyle.THICK);             
				style1.setBorderRight(BorderStyle.THICK);            
				style1.setBorderTop(BorderStyle.THICK);              
				style1.setBorderBottom(BorderStyle.THICK);
				style1.setAlignment(HorizontalAlignment.CENTER);
				style1.setVerticalAlignment(VerticalAlignment.CENTER);
	    	    
				items1="";
	        	quantity1="";
	        	TotalHarga2 = "";
            	
            	ttlHarga=0;
            	ttlQty=0;
            	
            	
			    for (int d = 0; d < result2.size(); d++) {
			    	String kode_ponya = result2.get(d).get("kode_po").toString();
			    	
			    	if(kode_po.matches(kode_ponya)){
			    		
			    		Map<String, Integer> frequencyMap = new HashMap<>();
		        		for (int f = 0; f < result2.size(); f++) {
		        			if(kode_po.matches(result2.get(f).get("kode_po").toString())){
			        			Integer count = frequencyMap.get(result2.get(f).get("kode_po").toString());
			        			if (count == null)
			        				count = 0;
			        			frequencyMap.put(kode_po, count + 1);
		        			}
		        		}

		        		String customer = result2.get(d).get("customer").toString();
		            	String alamat_cust = result2.get(d).get("alamat_cust").toString();
		            	
		        		for (Map.Entry<String, Integer> entry : frequencyMap.entrySet()) {
		        			if(kode_po.matches(entry.getKey())){
		        				noNya =  entry.getValue();
		        				
		        				if(noNya > d ){
		        					
		        					rowBarang = sheet.createRow( d + 2);
					        		
		        					
								    cell = rowBarang.createCell(1);
								    cell.setCellValue(numbers);
								    cell.setCellStyle(style);
								     
								    cell = rowBarang.createCell(2);
								    cell.setCellValue(kode_ponya);
								    cell.setCellStyle(style);
								    
								    cell = rowBarang.createCell(3);
								    cell.setCellValue(customer);
								    cell.setCellStyle(style);
								    
								    cell = rowBarang.createCell(4);
								    cell.setCellValue(alamat_cust);
								    cell.setCellStyle(style);
								    
								    if(d == noNya - 1){
								    sheet.addMergedRegion(new CellRangeAddress(2, noNya + 1, 1, 1));
								    sheet.addMergedRegion(new CellRangeAddress(2, noNya + 1, 2, 2));
								    sheet.addMergedRegion(new CellRangeAddress(2, noNya + 1, 3, 3));
								    sheet.addMergedRegion(new CellRangeAddress(2, noNya + 1, 4, 4));
								    
								    sheet.addMergedRegion(new CellRangeAddress(noNya + 2, noNya + 2, 1, 5));
		        					}
								    
								    
			        				items= "";
				         			hargas="";
				         			quantity="";
				         			TotalHarga="";
				         			
							    	quantity =  result2.get(d).get("jmlh_barang").toString() ;
					     			items =  result2.get(d).get("nama_barang").toString();
					                hargas = result2.get(d).get("harga_beli").toString(); 
								    
					                
			        				cell1 = rowBarang.createCell(5);
					                cell1.setCellValue(items);
					                cell1.setCellStyle(style);
								    
					                cell1 = rowBarang.createCell(6);
					                cell1.setCellValue(Integer.valueOf(quantity));
					                cell1.setCellStyle(style);	
					                
								    
					                cell1 = rowBarang.createCell(7);
					                cell1.setCellValue(Integer.valueOf(hargas));
					                cell1.setCellStyle(style);
								    
					                cell1 = rowBarang.createCell(8);
					                cell1.setCellStyle(style);
					                formula1= "(G" + (d + 3) + "*H" + (d + 3) + ")";
					                cell1.setCellFormula(formula1);
					                
					                noNyas12 = noNya + 2;
					                poKode = noNyas12;
					                
		        					formula = "SUM(G" + 2 + ":G" + (noNya + 2) + ")";
							    	formula2 = "SUM(I" + 2 + ":I" + (noNya + 2) + ")";
							    	
							    	row = sheet.createRow(noNya + 2);
								    
								    cellttl = row.createCell(1);;
								    cellttl.setCellValue("Total : ");
								    cellttl.setCellStyle(style1);
								    
								    cellttl = row.createCell(2);;
								    cellttl.setCellValue("");
								    cellttl.setCellStyle(style1);
								    
								    cellttl = row.createCell(3);;
								    cellttl.setCellValue("");
								    cellttl.setCellStyle(style1);
								    
								    cellttl = row.createCell(4);;
								    cellttl.setCellValue("");
								    cellttl.setCellStyle(style1);
								    
								    cellttl = row.createCell(5);;
								    cellttl.setCellValue("");
								    cellttl.setCellStyle(style1);
								    
								    cellttl = row.createCell(6);
								    cellttl.setCellStyle(style1); 
								    cellttl.setCellFormula(formula);
					                
								    cellttl = row.createCell(8);
								    cellttl.setCellStyle(style1);
								    cellttl.setCellFormula(formula2); 
								    
								    nilas1 = noNya + 2;
								    
			        			}else if(noNyas12 >= d ){
			        				
			        				rows = sheet.createRow(poKode + 1);
			        					
								    cell = rows.createCell(1);
								    cell.setCellValue(numbers);
								    cell.setCellStyle(style);
									
								    cell = rows.createCell(2);
								    cell.setCellValue(kode_ponya);
								    cell.setCellStyle(style);
									 
								    cell = rows.createCell(3);
								    cell.setCellValue(customer);
								    cell.setCellStyle(style);
									   
								    cell = rows.createCell(4);
								    cell.setCellValue(alamat_cust);
								    cell.setCellStyle(style);
			        				
					                items= "";
				         			hargas="";
				         			quantity="";
				         			TotalHarga="";
				         			
							    	quantity =  result2.get(d).get("jmlh_barang").toString();
					     			items =  result2.get(d).get("nama_barang").toString();
					                hargas = result2.get(d).get("harga_beli").toString(); 
								    
					                cell = rows.createCell(5);
					                cell.setCellValue(items);
					                cell.setCellStyle(style);
								    
					                cell = rows.createCell(6);
					                cell.setCellValue(Integer.valueOf(quantity));
					                cell.setCellStyle(style);	
					                
								    
					                cell = rows.createCell(7);
					                cell.setCellValue(Integer.valueOf(hargas));
					                cell.setCellStyle(style);
								    
					                cell = rows.createCell(8);
					                cell.setCellStyle(style);
					                formula1= "(G" + (poKode + 2) + "*H" + (poKode + 2) + ")";
					                cell.setCellFormula(formula1);
					                
								    if(d == (nilas + noNya + 2) && numbers1 == numbers){
								    	
								    	if(nilas == 0 && numbers1 == numbers){
								    		
								    		sheet.addMergedRegion(new CellRangeAddress(poKode + 2 - noNya, poKode  + 1, 1, 1));
										    sheet.addMergedRegion(new CellRangeAddress(poKode + 2 - noNya, poKode  + 1, 2, 2));
										    sheet.addMergedRegion(new CellRangeAddress(poKode + 2 - noNya, poKode  + 1, 3, 3));
										    sheet.addMergedRegion(new CellRangeAddress(poKode + 2 - noNya, poKode  + 1, 4, 4));
										    
								    		
								    		formula = "SUM(G" + (poKode + 2) + ":G" + (poKode + noNya - 1) + ")";
									    	formula2 = "SUM(I" + (poKode + 2) + ":I" + (poKode + noNya - 1) + ")";
									    	
								    		rowBarangs = sheet.createRow(poKode + 2);
								    		
								    		cellttl2 = rowBarangs.createCell(1);
									    	cellttl2.setCellValue("Total : ");
									    	cellttl2.setCellStyle(style1);
									    	
									    	cellttl2 = rowBarangs.createCell(2);;
									    	cellttl2.setCellValue("");
									    	cellttl2.setCellStyle(style1);
										    
									    	cellttl2 = rowBarangs.createCell(3);;
									    	cellttl2.setCellValue("");
									    	cellttl2.setCellStyle(style1);
										    
									    	cellttl2 = rowBarangs.createCell(4);;
									    	cellttl2.setCellValue("");
									    	cellttl2.setCellStyle(style1);
										    
									    	cellttl2 = rowBarangs.createCell(5);;
									    	cellttl2.setCellValue("");
									    	cellttl2.setCellStyle(style1);
										    
									    	cellttl2 = rowBarangs.createCell(6);
//									    	cellttl2.setCellValue(noNya);
									    	cellttl2.setCellStyle(style1); 
										    cellttl2.setCellFormula(formula);
							                
									    	cellttl2 = rowBarangs.createCell(8);
//									    	cellttl2.setCellValue(poKode);
									    	cellttl2.setCellStyle(style1);
										    cellttl2.setCellFormula(formula2); 
										   
										    sheet.addMergedRegion(new CellRangeAddress(poKode + 2, poKode + 2, 1, 5));
										    noNyas12+=1;
										    nilas += noNya;
										    nilas1 += noNya;
								    	} else if (nilas != 0 && numbers1 == numbers) {
								    		
								    		sheet.addMergedRegion(new CellRangeAddress(poKode + 2 - noNya, poKode  + 1, 1, 1));
										    sheet.addMergedRegion(new CellRangeAddress(poKode + 2 - noNya, poKode  + 1, 2, 2));
										    sheet.addMergedRegion(new CellRangeAddress(poKode + 2 - noNya, poKode  + 1, 3, 3));
										    sheet.addMergedRegion(new CellRangeAddress(poKode + 2 - noNya, poKode  + 1, 4, 4));
								    		
								    		formula = "SUM(G" + (poKode + 3 - noNya) + ":G" + (poKode + 2) + ")";
									    	formula2 = "SUM(I" + (poKode + 3 - noNya) + ":I" + (poKode + 2) + ")";
									    	
									    	rowBarangs = sheet.createRow(poKode + 2);
									    	
									    	cellttl2 = rowBarangs.createCell(1);
									    	cellttl2.setCellValue("Total : ");
									    	cellttl2.setCellStyle(style1);
									    	
									    	cellttl2 = rowBarangs.createCell(2);;
									    	cellttl2.setCellValue("");
									    	cellttl2.setCellStyle(style1);
										    
									    	cellttl2 = rowBarangs.createCell(3);;
									    	cellttl2.setCellValue("");
									    	cellttl2.setCellStyle(style1);
										    
									    	cellttl2 = rowBarangs.createCell(4);;
									    	cellttl2.setCellValue("");
									    	cellttl2.setCellStyle(style1);
										    
									    	cellttl2 = rowBarangs.createCell(5);;
									    	cellttl2.setCellValue("");
									    	cellttl2.setCellStyle(style1);
										    
									    	cellttl2 = rowBarangs.createCell(6);
//									    	cellttl2.setCellValue(noNya);
									    	cellttl2.setCellStyle(style1); 
										    cellttl2.setCellFormula(formula);
							                
									    	cellttl2 = rowBarangs.createCell(8);
//									    	cellttl2.setCellValue(poKode);
									    	cellttl2.setCellStyle(style1);
										    cellttl2.setCellFormula(formula2); 
										   
										    
										    sheet.addMergedRegion(new CellRangeAddress(poKode + 2, poKode + 2, 1, 5));
										    noNyas12+=1;
										    nilas += noNya;
										    nilas1 += noNya;
								    	}
								    	
								    	formula = "";
								    	formula2 = "";
								    	
								    	numbers1 = numbers + 1;

						            }
								    
					                poKode = noNyas12 + 1 ;
					                noNyas12++;
			        			 } 
		        										    
							    ttlQty1 += Integer.valueOf(result2.get(d).get("jmlh_barang").toString());
							    ttlHarga1 += Integer.valueOf(result2.get(d).get("jmlh_barang").toString()) * Integer.valueOf(result2.get(d).get("harga_beli").toString());

		        			}  
		        			
		        		
		        		}
		        		
			    	}

			    }
			    
			    
			}
			
		    
			kodePO = kode_po;
	    }
	    

	    
	    styles1 = workbook.createCellStyle();
	    org.apache.poi.ss.usermodel.Font font = workbook.createFont();
        font.setColor(HSSFColor.HSSFColorPredefined.BLUE.getIndex());
        styles1.setFont(font);
        /* First, let us draw a thick border so that the color is visible */            
	    styles1.setBorderLeft(BorderStyle.THICK);             
	    styles1.setBorderRight(BorderStyle.THICK);            
	    styles1.setBorderTop(BorderStyle.THICK);              
	    styles1.setBorderBottom(BorderStyle.THICK);
	    styles1.setAlignment(HorizontalAlignment.CENTER);
	    styles1.setVerticalAlignment(VerticalAlignment.CENTER);
	    
	    
	    rowBarangs1 = sheet.createRow(rowBarangs.getRowNum() + 2);
	    formula = "";
	    formula2 = "";
	    
	    cellttl = rowBarangs1.createCell(1);;
	    cellttl.setCellValue("Total All : ");
	    cellttl.setCellStyle(styles1);
	    
	    cellttl = rowBarangs1.createCell(2);;
	    cellttl.setCellValue("");
	    cellttl.setCellStyle(styles1);
	    
	    cellttl = rowBarangs1.createCell(3);;
	    cellttl.setCellValue("");
	    cellttl.setCellStyle(styles1);
	    
	    cellttl = rowBarangs1.createCell(4);;
	    cellttl.setCellValue("");
	    cellttl.setCellStyle(styles1);
	    
	    cellttl = rowBarangs1.createCell(5);;
	    cellttl.setCellValue("");
	    cellttl.setCellStyle(styles1);
	    
	    cellttl = rowBarangs1.createCell(6);;
	    cellttl.setCellValue(ttlQty1);
	    cellttl.setCellStyle(styles1);

	    cellttl = rowBarangs1.createCell(8);
	    cellttl.setCellValue(ttlHarga1);
	    cellttl.setCellStyle(styles1);

	    sheet.addMergedRegion(new CellRangeAddress(rowBarangs.getRowNum() + 2, rowBarangs.getRowNum() + 2, 1, 5));
	    
	    // Remove Empty Rows 
//	    removeEmptyRows(sheet);
	    
	    try (FileOutputStream outputStream = new FileOutputStream(pathFile1)) {
	        workbook.write(outputStream);
	    }
		
	    return pathXLS;
	}
	
	private  void createHeaderRow(Sheet sheet) {
		 
	    CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
	    org.apache.poi.ss.usermodel.Font font = sheet.getWorkbook().createFont();
	    font.setBold(true);
	    font.setFontHeightInPoints((short) 16);
	    cellStyle.setFont(font);
	 
	    Row row = sheet.createRow(1);
	    CellStyle style = workbook.createCellStyle();
	    style.setBorderLeft(BorderStyle.THICK);             
	    style.setBorderRight(BorderStyle.THICK);            
	    style.setBorderTop(BorderStyle.THICK);              
	    style.setBorderBottom(BorderStyle.THICK);
        
        /* We will use IndexedColors to specify colors to the border */
        /* bottom border color */
	    style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        /* Top border color */
	    style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        /* Left border color */
	    style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        /* Right border color */
	    style.setRightBorderColor(IndexedColors.BLACK.getIndex());
	    
	    Cell cellTitle = row.createCell(1);
	    cellTitle.setCellStyle(cellStyle);
	    cellTitle.setCellValue("No");
	    cellTitle.setCellStyle(style);
	    Cell cellAuthor = row.createCell(2);
	    cellAuthor.setCellStyle(cellStyle);
	    cellAuthor.setCellValue("Kode PO");
	    cellAuthor.setCellStyle(style);
	    Cell cellPrice = row.createCell(3);
	    cellPrice.setCellStyle(cellStyle);
	    cellPrice.setCellValue("Customer");
	    cellPrice.setCellStyle(style);
	    Cell cellPrice1 = row.createCell(4);
	    cellPrice1.setCellStyle(cellStyle);
	    cellPrice1.setCellValue("Alamat");
	    cellPrice1.setCellStyle(style);
	    Cell cellPrice2 = row.createCell(5);
	    cellPrice2.setCellStyle(cellStyle);
	    cellPrice2.setCellValue("Nama Barang");
	    cellPrice2.setCellStyle(style);
	    Cell cellPrice3 = row.createCell(6);
	    cellPrice3.setCellStyle(cellStyle);
	    cellPrice3.setCellValue("Quantity");
	    cellPrice3.setCellStyle(style);
	    Cell cellPrice4 = row.createCell(7);
	    cellPrice4.setCellStyle(cellStyle);
	    cellPrice4.setCellValue("Harga");
	    cellPrice4.setCellStyle(style);
	    Cell cellPrice5 = row.createCell(8);
	    cellPrice5.setCellStyle(cellStyle);
	    cellPrice5.setCellValue("Total Harga");
	    cellPrice5.setCellStyle(style);
	}
	
	private  void removeEmptyRows(Sheet sheet) {
	    Boolean isRowEmpty = Boolean.FALSE;
	    for(int i = 1; i <= sheet.getLastRowNum(); i++){
	      if(sheet.getRow(i)==null){
	        isRowEmpty=true;
	        sheet.shiftRows(i + 1, sheet.getLastRowNum()+1, -1);
	        i--;
	        continue;
	      }
	      for(int j =1; j<sheet.getRow(i).getLastCellNum();j++){
	        if(sheet.getRow(i).getCell(j) == null || 
	        sheet.getRow(i).getCell(j).toString().trim().equals("")){
	          isRowEmpty=true;
	        }else {
	          isRowEmpty=false;
	          break;
	        }
	      }
	      if(isRowEmpty==true){
	        sheet.shiftRows(i + 1, sheet.getLastRowNum()+1, -1);
	        i--;
	      }
	    }
	  }
}
