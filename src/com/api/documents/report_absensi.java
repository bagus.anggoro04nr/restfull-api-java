/**
 * 
 */
package com.api.documents;

import java.awt.HeadlessException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

import com.api.libs.MonthNames;
import com.api.libs.pathfile;
import com.api.libs.servers;
import com.api.libs.setgetdate;
import com.api.libs.typesnya;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @author Bagus Anggoro
 *
 */
public class report_absensi {
	
	private String pathPDF="",  pathFile="";
	private Document document;
	private String namaFiles="";
	private PdfWriter writer;
	private PdfPTable table, table1, table2, table3;
	private Font boldFont, StandardFont, TotalFont;
	private MonthNames monthName;
	private String Years, niKS;
	private List<Map<String, Object>> monthNamenya;
	private int nourut;
	
	public report_absensi() {
		super();
		monthName = new MonthNames();
		Years="";
		niKS="";
		nourut=0;
	}
	
	private  String namaFile(String types){
		
		
			namaFiles = "Reports_" + types + "_" + setgetdate.getMonth() + "_" + setgetdate.getYear();
		
		
		return namaFiles;
		 
	}
	
	public  Object createToPDF(List<Map<String, Object>> result) throws DocumentException, JSONException, SQLException, HeadlessException, UnsupportedFlavorException, IOException {
		pathPDF = "";
		document = new Document();
		pathFile = pathfile.getPathFile() + namaFile(typesnya.getType().toUpperCase()) + ".pdf".trim();
		pathPDF = servers.getHostname().toString() + pathfile.getPathFile1() + namaFile(typesnya.getType().toUpperCase()) + ".pdf".trim();
//		pathPDF = pathPDF.replace("/../", "");
		
		
		Years = setgetdate.getYear();
		monthNamenya = monthName.setGetMontNames(setgetdate.getMonth());
		
		writer = PdfWriter.getInstance(document, new FileOutputStream(pathFile));
		
//		writer.setEncryption(USER_PASSWORD.getBytes(),
//                OWNER_PASSWORD.getBytes(), PdfWriter.ALLOW_PRINTING,
//                PdfWriter.ENCRYPTION_AES_128);
		
	    document.open();
	    document.add(new Paragraph("Reports Print Date : " + setgetdate.giveDateNowReport()));
	    document.add(new Paragraph(""));
	    document.add(new Paragraph("Reports " + typesnya.getType().toUpperCase()));
	    
	    //Set attributes here
	    document.addAuthor("iseng - iseng");
	    document.addCreationDate();
	    document.addCreator("main aja");
	    document.addTitle("Reports");
	    document.addSubject("Report" + typesnya.getType().toUpperCase());
        
        //Set Column widths
	   
        float[] columnWidths1 = {1, 5, 5};
        float[] columnWidths2 = {1, 5, 1f, 1f, 1f, 1f, 1f};
        float[] columnWidths3 = {1f, 1f, 1f, 1f, 1f};
        float[] totalcolumnWidths = {1f, 1f, 1f, 1f};
        
        boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
        StandardFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
        TotalFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLUE );
	    
        table2 = new PdfPTable(columnWidths3); // 6 columns.
        table2.setWidthPercentage(100); //Width 100%
        table2.setSpacingBefore(5f); //Space before table
        table2.setSpacingAfter(5f); //Space after table
        table2.setWidths(columnWidths3);
        table2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table2.getDefaultCell().setPaddingLeft(5);
        table2.getDefaultCell().setBorderColor(BaseColor.BLACK);
        table2.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.getDefaultCell().setBorder(0);
        table2.addCell(new Phrase("Hadir", boldFont ));
        table2.addCell(new Phrase("Ijin", boldFont ));
        table2.addCell(new Phrase("Alpha", boldFont ));
        table2.addCell(new Phrase("Grade", boldFont ));
        table2.addCell(new Phrase("Point", boldFont ));

        System.out.println(monthNamenya.get(0).size());
		
        for (int a = 0; a < monthNamenya.get(0).size(); a++)  {
        	
        	
        		
		        	if(a >= 1) {
		        		
		        		table = new PdfPTable(columnWidths1); // 6 columns.
		                table.setWidthPercentage(100); //Width 100%
		                table.setSpacingBefore(5f); //Space before table
		                table.setSpacingAfter(5f); //Space after table
		                table.setWidths(columnWidths1);
		                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		                table.getDefaultCell().setPaddingLeft(5);
		                table.getDefaultCell().setBorderColor(BaseColor.BLACK);
		                table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		                table.getDefaultCell().setBorder(1);
		        	    table.addCell(new Phrase("NO", boldFont ));
		        	    table.addCell(new Phrase("Nama", boldFont ));
		        	    table.addCell(new Phrase(monthNamenya.get(0).get(String.valueOf(a)).toString(), boldFont ));
		        	    
		        	    document.add(table);
		        		
		        	    table1 = new PdfPTable(columnWidths1); // 6 columns.
		        	    table1.setWidthPercentage(100); //Width 100%
		        	    table1.setSpacingBefore(5f); //Space before table
		        	    table1.setSpacingAfter(5f); //Space after table
		        	    table1.setWidths(columnWidths1);
		        	    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		        	    table1.getDefaultCell().setPaddingLeft(5);
		        	    table1.getDefaultCell().setBorderColor(BaseColor.BLACK);
		        	    table1.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		        	    table1.getDefaultCell().setBorder(1);
		        	    table1.addCell(new Phrase("", boldFont ));
		        	    table1.addCell(new Phrase("", boldFont ));
		        	    table1.addCell(table2);
		            	
		        	    document.add(table1);
		        	    for (int i = 0; i < result.size(); i++)  {
		            		
		        	    String niks = result.get(i).get("niks").toString();	
		        	    if(!niks.matches(niKS)){
			        		nourut++;
		        	    table3 = new PdfPTable(columnWidths2); // 6 columns.
		        	    table3.setWidthPercentage(100); //Width 100%
		        	    table3.setSpacingBefore(5f); //Space before table
		        	    table3.setSpacingAfter(5f); //Space after table
		        	    table3.setWidths(columnWidths2);
		        	    table3.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		        	    table3.getDefaultCell().setPaddingLeft(5);
		        	    table3.getDefaultCell().setBorderColor(BaseColor.BLACK);
		        	    table3.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		        	    table3.getDefaultCell().setBorder(1);
		        	    table3.addCell(new Phrase(String.valueOf(nourut), boldFont ));
		        	    table3.addCell(new Phrase(result.get(i).get("namakaryawans").toString(), boldFont ));
		        	    
		        	    if(monthNamenya.get(0).get(String.valueOf(a)).toString().contains(result.get(i).get("bulan").toString())) {
			        	    
			        	    table3.addCell(new Phrase(result.get(i).get("masuk").toString(), boldFont ));
			        	    table3.addCell(new Phrase(result.get(i).get("ijin").toString(), boldFont ));
			        	    table3.addCell(new Phrase(result.get(i).get("alpha").toString(), boldFont ));
			        	    table3.addCell(new Phrase(result.get(i).get("grade").toString(), boldFont ));
			        	    table3.addCell(new Phrase(result.get(i).get("points").toString(), boldFont ));
		        	    }else {
		        	    	table3.addCell(new Phrase(String.valueOf(0), boldFont ));
			        	    table3.addCell(new Phrase(String.valueOf(0), boldFont ));
			        	    table3.addCell(new Phrase(String.valueOf(0), boldFont ));
			        	    table3.addCell(new Phrase("-", boldFont ));
			        	    table3.addCell(new Phrase("-", boldFont ));
		        	    }
		        	    document.add(table3);
		        	    }niKS = niks; 
		        	    }nourut=0;
		        	}else {
		        		
		        		table = new PdfPTable(columnWidths1); // 6 columns.
		                table.setWidthPercentage(100); //Width 100%
		                table.setSpacingBefore(5f); //Space before table
		                table.setSpacingAfter(5f); //Space after table
		                table.setWidths(columnWidths1);
		                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		                table.getDefaultCell().setPaddingLeft(5);
		                table.getDefaultCell().setBorderColor(BaseColor.BLACK);
		                table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		                table.getDefaultCell().setBorder(1);
		        	    table.addCell(new Phrase("NO", boldFont ));
		        	    table.addCell(new Phrase("Nama", boldFont ));
		        	    table.addCell(new Phrase(monthNamenya.get(0).get(String.valueOf(a)).toString(), boldFont ));
		        	    
		        	    document.add(table);
		        		
		        	    table1 = new PdfPTable(columnWidths1); // 6 columns.
		        	    table1.setWidthPercentage(100); //Width 100%
		        	    table1.setSpacingBefore(5f); //Space before table
		        	    table1.setSpacingAfter(5f); //Space after table
		        	    table1.setWidths(columnWidths1);
		        	    table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		        	    table1.getDefaultCell().setPaddingLeft(5);
		        	    table1.getDefaultCell().setBorderColor(BaseColor.BLACK);
		        	    table1.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		        	    table1.getDefaultCell().setBorder(1);
		        	    table1.addCell(new Phrase("", boldFont ));
		        	    table1.addCell(new Phrase("", boldFont ));
		        	    table1.addCell(table2);
		        	    document.add(table1);
		        	    
		        	    for (int i = 0; i < result.size(); i++)  {
		            		
		            	String niks = result.get(i).get("niks").toString();	
		        	    if(!niks.matches(niKS)){
			        		nourut++;
		        	    table3 = new PdfPTable(columnWidths2); // 6 columns.
		        	    table3.setWidthPercentage(100); //Width 100%
		        	    table3.setSpacingBefore(5f); //Space before table
		        	    table3.setSpacingAfter(5f); //Space after table
		        	    table3.setWidths(columnWidths2);
		        	    table3.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		        	    table3.getDefaultCell().setPaddingLeft(5);
		        	    table3.getDefaultCell().setBorderColor(BaseColor.BLACK);
		        	    table3.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		        	    table3.getDefaultCell().setBorder(1);
		        	    table3.addCell(new Phrase(String.valueOf(nourut), boldFont ));
		        	    table3.addCell(new Phrase(result.get(i).get("namakaryawans").toString(), boldFont ));

		        	    if(monthNamenya.get(0).get(String.valueOf(a)).toString().contains(result.get(i).get("bulan").toString())) {
			        	    
			        	    table3.addCell(new Phrase(result.get(i).get("masuk").toString(), boldFont ));
			        	    table3.addCell(new Phrase(result.get(i).get("ijin").toString(), boldFont ));
			        	    table3.addCell(new Phrase(result.get(i).get("alpha").toString(), boldFont ));
			        	    table3.addCell(new Phrase(result.get(i).get("grade").toString(), boldFont ));
			        	    table3.addCell(new Phrase(result.get(i).get("points").toString(), boldFont ));
		        	    }else {
		        	    	table3.addCell(new Phrase(String.valueOf(0), boldFont ));
			        	    table3.addCell(new Phrase(String.valueOf(0), boldFont ));
			        	    table3.addCell(new Phrase(String.valueOf(0), boldFont ));
			        	    table3.addCell(new Phrase("-", boldFont ));
			        	    table3.addCell(new Phrase("-", boldFont ));
		        	    }
		        	    document.add(table3);
		        	    }niKS = niks; 
		        	    }nourut=0;
		        	}
		        	
		        
        	}

        
        document.close();
        writer.close();
        
        return pathPDF;
	}

}
